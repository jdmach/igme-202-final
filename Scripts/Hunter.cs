﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Jordan Machalek
 * Handles movement behavior for a hunter in the simulation
 * Exhibits path following behavior
 */
public class Hunter : Vehicle {

    //Attributes
    private PathFollow followScript;
    public float pathWeight;

    // Use this for initialization
    public override void Start ()
    {
        base.Start();

        followScript = gameObject.GetComponent<PathFollow>();
    }

    //Generates movement forces to guide vehicle
    public override void CalcSteeringForces()
    {
        //Reset force
        ultimateForce = Vector3.zero;

        //Keep the vehicle in bounds
        ultimateForce += (StayInBounds() * boundsWeight);
        
        //Path follow
        ultimateForce += (Seek(followScript.WaypointCheck(vehiclePosition)) * pathWeight);

        //Check for collision with an area of resistance and apply force if so
        foreach (GameObject ob in fluids)
        {
            ultimateForce += (Drag(ob) * dragWeight);
        }

        //Limit force
        ultimateForce.Normalize();
        ultimateForce = ultimateForce * maxForce;

        //Apply force to acceleration
        ApplyForce(ultimateForce);
    }
}
