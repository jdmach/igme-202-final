﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Jordan Machalek
 * Stores a list of "waypoint" GameObjects for a path to be followed by a vehicle
 */
public class PathFollow : MonoBehaviour {

    //Attributes
    public List<GameObject> pointList;
    public GameObject nextWaypoint; // waypoint the vehicle is currently seeking
    private int waypointIndex;

	// Use this for initialization
	void Start ()
    {
        pointList = GameObject.Find("SceneManager").GetComponent<SceneManager>().waypoints;

        nextWaypoint = pointList[0];
        waypointIndex = 0;
	}

    //Returns the position of the current waypoint and advances to the next when reached
    //Format for use: ultimateForce += (Seek(WaypointCheck(vehiclePosition)) * pathWeight);
    public Vector3 WaypointCheck(Vector3 position)
    {
        Vector3 distToWaypoint = position - nextWaypoint.transform.position;

        //Check if the waypoint is reached
        if(distToWaypoint.sqrMagnitude < 9)
        {
            //Advance index
            waypointIndex++;

            //Check if the last waypoint is reached, loop back to start
            if(waypointIndex >= pointList.Count)
            {
                waypointIndex = 0;
            }

            //Set waypoint
            nextWaypoint = pointList[waypointIndex];
        }

        return nextWaypoint.transform.position;
    }
}
